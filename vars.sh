#!/bin/bash

export CONFIG_SERVER_PORT=8888
export CONFIG_SERVER_URI=https://gitlab.com/jesuscruz/config.git
export CONFIG_SERVER_USER=user
export CONFIG_SERVER_PASSWORD=password
export CONFIG_DEFAULT_LABEL=master
export POSTGRES_DB_USER=postgres
export POSTGRES_DB_PASSWORD=secret
export POSTGRES_DB_NAME=academy
export H2_DB_USER=sa
export H2_DB_PASSWORD=password
export H2_DB_NAME=test